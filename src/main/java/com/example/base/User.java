package com.example.base;

import java.util.Objects;

public class User {
    private String name;
    private String surname;
    private int id;
    private static int FIRST_ID = 0;

    public User() {
        //id = makeId();
    }

    public User(String name, String surname) {
        setName(name);
        setSurname(surname);
        //id = makeId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int makeId() {
        int id = FIRST_ID++;
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getName().equals(user.getName()) &&
                getSurname().equals(user.getSurname());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname());
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
