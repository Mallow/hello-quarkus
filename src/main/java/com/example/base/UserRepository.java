package com.example.base;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class UserRepository {
    List<User> userList = new ArrayList<>();
    int iter;

    public int getNextUSerId() {
        return iter++;
    }

    public List<User> findAll() {
        return userList;
    }

    public User findUserById(int id) {
        User tmp = new User();
        for (User u : userList) {
            if (u.getId() == id) {
                tmp = u;
            }
        }
        return tmp;
    }

    public void updateUser(User user) {
        User userUpdated = findUserById(user.getId());
        userUpdated.setName(user.getName());
        userUpdated.setSurname(user.getSurname());
    }

    public void createUser(User user) {
        user.setId(getNextUSerId());
        findAll().add(user);
    }

    public void deleteUser(int userId) {
        User u = findUserById(userId);
        findAll().remove(u);
    }
}
