package com.example;

import com.example.base.User;
import com.example.base.UserRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/persons")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RestEndpoint {

    @Inject
    UserRepository userRepository;

    @GET
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @POST
    public Response create(User user) {
        userRepository.createUser(user);
        return Response.status(201).build();
    }

    @PUT
    public Response update(User user) {
        userRepository.updateUser(user);
        return Response.status(204).build();
    }

    @DELETE
    public Response delete(@QueryParam("id") int userId) {
        userRepository.deleteUser(userId);
        return Response.status(204).build();
    }

}
